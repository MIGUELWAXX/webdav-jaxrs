~~~
~~ #%L
~~ WebDAV Support for JAX-RS
~~ %%
~~ Copyright (C) 2008 - 2014 The java.net WebDAV Project
~~ %%
~~ This program is free software: you can redistribute it and/or modify
~~ it under the terms of the GNU General Public License as
~~ published by the Free Software Foundation, either version 3 of the 
~~ License, or (at your option) any later version.
~~ 
~~ This program is distributed in the hope that it will be useful,
~~ but WITHOUT ANY WARRANTY; without even the implied warranty of
~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
~~ GNU General Public License for more details.
~~ 
~~ You should have received a copy of the GNU General Public 
~~ License along with this program.  If not, see
~~ <http://www.gnu.org/licenses/gpl-3.0.html>.
~~ #L%
~~~
Configuration

* Enabling WebDAV (mandatory)

  Your JAX-RS application must implement the <getSingletons()> optional method of the <javax.ws.rs.core.Application> interface to tell your JAX-RS implementation that WebDAV shall get used:
  
---
public Set<Object> getSingletons() {
	try {
		return new HashSet<Object>(Arrays.asList(new WebDavContextResolver(), new PersistenceExceptionMapper()));
	} catch (JAXBException e) {
		return null;
	}
}
---

  <<Note:>> Depending on the way of deployment and your particular JAX-RS implementation, it is very likely that you must tell your runtime engine in some way that this particular application class is to be used. For example, Jersey will use a different (default) class otherwise which will result in WebDAV not working properly. Please also check the deployment chapter of the JAX-RS specification for information.

* Enabling Custom Extensions (optional)

  If your application wants to use custom WebDAV XML extensions (like its own properties) those must be JAXB elements and have to get registered with the WebDAV runtime. To achieve this, pass the class names of your extensions to the constructor of the <WebDavContextResolver> instance:

---
return new HashSet<Object>(Arrays.asList(new WebDavContextResolver(Win32LastAccessTime.class),
---

---
@XmlRootElement(name = "Win32LastAccessTime", namespace = "urn:schemas-microsoft-com:")
public final class Win32LastAccessTime {

	@XmlValue
	private String content;

	/**
	 * @return Client specific string which is not to be further parsed, according to Microsoft's documentation.
	 * @see <a href="http://msdn.microsoft.com/en-us/library/cc250144(PROT.10).aspx">Chapter 2.2.10.8 "Z:Win32LastAccessTime Property" of MS-WDVME "Web Distributed Authoring and Versioning (WebDAV) Protocol: Microsoft Extensions"</a>
	 */
	public final String getContent() {
		return this.content;
	}

	/**
	 * @return "Win32LastAccesstime (content)"
	 */
	@Override
	public final String toString() {
		return String.format("Win32LastAccessTime (%s)", this.content);
	}

}
---
